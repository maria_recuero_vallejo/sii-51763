#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

 
int main(){

	int fd,val=0;
	const char*myFifo="/tmp/fifo";
	char buf[200];

 	mkfifo(myFifo,0666);
	fd=open(myFifo,O_RDONLY);

	while(val!=-1){

	val=read(fd, buf, sizeof(buf));
	printf("%s\n", buf);

 	if(buf[0]=='F'||val==-1)val=-1;//comprobamos errores y salimos si se cumple la anterior condición
    	}

 
     	close(fd);
     	unlink(myFifo);
     	return 0;
 }
