#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"


int main(){

	int file;
	DatosMemCompartida* pMemComp;
	const char*myBot="/tmp/bot";

	file=open("/tmp/bot",O_RDWR);
	pMemComp=(DatosMemCompartida*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);
	close(file);
	
	int salir=0;
	while(salir==0)
	{
		if(pMemComp->accion==2)
			salir=1;
		usleep(25000);
		float posRaqueta;
		
		posRaqueta=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		
		if(posRaqueta<pMemComp->esfera.centro.y)
			pMemComp->accion=1;
		else if(posRaqueta>pMemComp->esfera.centro.y)
			pMemComp->accion=-1;
		else
			pMemComp->accion=0;
	}

	/*while(pMemComp->accion1!=2)
	{
		usleep(25000);  
		float posRaqueta1;
		
		posRaqueta1=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		
		if(posRaqueta1<pMemComp->esfera.centro.y)
			pMemComp->accion1=1;
		else if(posRaqueta1>pMemComp->esfera.centro.y)
			pMemComp->accion1=-1;
		else
			pMemComp->accion1=0;		
	}*/


	munmap(pMemComp,sizeof(*(pMemComp)));
}
